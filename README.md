# OVERVIEW
Imagine breaking free of the standard 4 dialog options in roleplaying videogames. Ask whatever you want to an NPC, with total freedom and have a natural conversation with them.

The goal of this project is to create an implementation of Large Language Models to achive realistic and completely interactive NPC in videogames.

# Project Status
Very much Work In Progress.
